def transform_decrees(text, **kwargs):
    """
    Create custom tokens for decrees appearances in a text

    :param text: Text which will have its decrees tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the decrees tokens created
    """

    decree_types = (
        "leis?",
        "regulamentador(?:es)?",
        "presidencia(?:l|is)",
        "federa(?:l|is)",
        "estadua(?:l|is)",
        "executivos?",
        "leis? complementar(?:es)?",
        "municipa(?:l|is)",
        "legislativos?",
        "legislativos? estadua(?:l|is)",
        "legislativos? municipa(?:l|is)",
    )
    
    # Tipos de decretos separados por |
    decree_types = "|".join(decree_types)

    # n / n. / no / nº / no. / nº.
    # nº / n.º
    # número / numero
    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"
    
    # digito\digito / digito.digito / digito,digito
    number = r"(?:\d+(?:[\.,]\d+)*)"
    
    # Opcional: /number / -number
    year = fr"(?:[/-]{number})?"
    
    prefix_result = "DECRETO_" 
    
    # ST_DECRETO_number
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    
    # Never or more: " " (whitespace)
    #  , (whitespace, one or more) / e (whitespace, one or more)
    separator = r"\s*[,e]\s+"
    
    # Grupo identification
    decree = fr"(?P<identification>{number}){year}"  
    
    # Flags = re.I -> ignora case
    # Depois de ignored_expression: decreto(s)[\s-]+tipo_decreto (whitespace)+word_number(whitespace)*decreto
    # Obs: []+ um dos dentro do[], uma ou mais vezes / *, nenhum ou mais vezes
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(decretos?([\s-]+({decree_types}))?)\s+(?:{word_number}\s*)?{decree}",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        decree,
        previous_token,
        separator,
    )



def transform_amendments(text, **kwargs):
    """
    Create custom tokens for amendments appearances in a text

    :param text: Text which will have its admendments tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the admendments tokens created
    """

    amendment_types = ("constituciona(?:l|is)", "regimenta(?:l|is)")
    
    # Tipos de emendas separados por |
    amendment_types = "|".join(amendment_types)

    # n / n. / no / nº / no. / nº.
    # nº / n.º
    # número / numero
    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"
    
    # digito\digito / digito.digito / digito,digito
    number = r"(?:\d+(?:[\.,]\d+)*)"
    
    # Opcional: /number / -number
    year = fr"(?:[/-]{number})?"
    
    prefix_result = "EMENDA_"
    
    # ST_EMENDA_number
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    
    # Never or more: " " (whitespace)
    #  , (whitespace, one or more) / e (whitespace, one or more)
    separator = r"\s*[,e]\s+"
    
    amendment = fr"(?P<identification>{number}){year}"
    
    # Flags = re.I -> ignora case
    # Depois de ignored_expression: emenda(s)(whitespace)+tipo_emenda(whitespace)+word_number(whitespace)*emenda
    # Obs: +, uma ou mais vezes / *, nenhum ou mais vezes
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(emendas?(\s+({amendment_types}))?)\s+(?:{word_number}\s*)?{amendment}",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        amendment,
        previous_token,
        separator,
    )


def transform_articles(text, **kwargs):
    """
    Create custom tokens for articles appearances in a text.\n
    It also works to create tokens for the articles items and indents

    :param text: Text which will have its articles tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the articles tokens created
    """

    # n / n. / no / nº / no. / nº.
    # nº / n.º
    # número / numero
    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"

    # digito\digito / digito.digito / digito,digito
    number = r"(?:\d+(?:[\.,]\d+)*)"
    
    prefix_result = "ARTIGO_"
    
    #ST_ARTIGO_number
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    
    # Never or more: " " (whitespace)
    #  , (whitespace, one or more) / e (whitespace, one or more)
    separator = r"\s*[,e]\s+"
    
    # numberº(opcional)-(opcional)caracter+
    # +, uma vez ou mais
    article = fr"(?P<article>{number}º?(?:-?\w+)?)"
    
    # Flags = re.I -> ignora case
    # art(s)(.)|artigo(s)(whitespace)+word_number(whitespace)*{article}
    # Obs: +, uma ou mais vezes / *, nenhum ou mais vezes
    re_article = re.compile(
        fr"(?:arts?\.?|artigos?)\s+(?:{word_number}\s*)?{article}", 
        flags=re.I
    )

    def build_token(m):
        result = (
            ST_PREFIX
            + prefix_result
            + m.group("article")
            .replace("-", "")
            .replace("º", "")
            .replace(".", "")
            .replace(",", "")
            .lstrip("0")
            .upper()
        )
        return result

    # replaces the first article occurences
    text = re_article.sub(build_token, text) # 1-> ST_ARTIGO_N°

    text = _transform_article_items(text) # 2-> ST_INCISO_N° 
    text = _transform_article_indents(text) # 3-> ST_ALINEA_c
    text = transform_item_indents(text) # 4

def transform_emails(text, **kwargs):
    """
    Create custom tokens for email appearances in a text.\n
    The composition of e-mail is local_part@domain.domain

    :param text: Text which will have its emails tokens created
    :type text: str
    :rtype: str
    :return: Text with the emails token created (from local_part@domain.domain to ST_EMAIL)

    *Note: Not good enough to validate an e-mail*
    """
    
    # Basicamente todos os caracteres, menos os mais "incomuns"
    local_part = r"[\w!#$%&'*+\-/=?^_`{|}~.]+"
    
    # Novamente, quase todos os caracteres, mas detectando o "."
    domain = r"[a-zA-Z][\w\-]*(\.\w[\w\-]+)+"

    # Substitui o e-mail por ST_EMAIL (sem contar case)
    return re.sub(
        fr"{local_part}@{domain}", f"{ST_PREFIX}EMAIL", text, flags=re.I
    )

def transform_syn(
    text,
    synonyms_list=None,
    synonyms_dict=None,
    synonyms_re=None,
    is_preprocessed=True,
    special_laws=False,
    **kwargs,
):
    """
    Create custom tokens for synonyms words appearances in a text based in a static list. Basically, some common words can have synonyms in the text, so this function create tokens for these words.\n
    
    If the argument **synonyms_list** isn't passed in, a default .txt file with the list is needed.\n
    If the argument **synonyms_dict** isn't passed in, the dict will be created based from the synonyms_list.\n
    If the argument **synonyms_re** isn't passed in, the regex will be created based from the synonyms_dict.\n

    :param text: Text which will have its words analyzed and maybe tokens of synonyms created
    :type text: str
    :param synonyms_list: list of synonyms words (default is None)
    :type synonyms_list: list, optional
    :param synonyms_dict: dict of synonyms words (default is None)
    :type synonyms_dict: dict, optional
    :param synonyms_re: regex of synonyms (default is None)
    :type synonyms_re: str, optional
    :param is_preprocessed: if the text is already preprocessed (default is True)
    :type is_preprocessed: bool, optional
    :param special_laws: if the synonyms are related to special laws or "common words" (default is False)
    :type special_laws: bool, optional
    :param kwargs: is_text_preprocessed -- Condition if text is already preprocessed (default True), so synonyms will as well
    :rtype: str
    :return: The text with the synonyms tokens created (synonyms words with their tokens) will be returned
    """
    
    if synonyms_dict is None:
        synonyms_dict = get_synonyms_dict(
            synonyms_list=synonyms_list,
            is_preprocessed=is_preprocessed,
            special_laws=special_laws,
        )

    def replace(m):
        token = m.group(1)

        try:
            return synonyms_dict[token.lower().replace("\n", " ")]
        except KeyError:
            logging.debug(
                "'%s' token doesn't have synonym but was caught in"
                " text '%s'",
                token,
                text,
            )
            print(token)
            return token

    if synonyms_re is None:
        synonyms_re = get_synonyms_re(
            synonyms_list, synonyms_dict, is_preprocessed, special_laws
        )

    return synonyms_re.sub(replace, text)

def transform_special_laws(
    text,
    special_laws_list=None,
    special_laws_dict=None,
    special_laws_re=None,
    is_preprocessed=True,
    **kwargs,
):
    """
    Create custom tokens for special laws appearances in a text based in a static list. Basically, some common special laws can have synonyms (different words to represent them) in the text, so this function calls transform_syn with special_laws set as True so tokens for these special laws can be created.\n
    
    If the argument **special_laws_list** isn't passed in, a default .txt file with the list is needed.\n
    If the argument **special_laws_dict** isn't passed in, the dict will be created based from the special_laws_list.\n
    If the argument **special_laws_re** isn't passed in, the regex will be created based from the special_laws_dict.\n

    :param text: Text which will have its words analyzed and maybe tokens of special laws created
    :type text: str
    :param special_laws_list: list of special laws (default is None)
    :type special_laws_list: list, optional
    :param special_laws_dict: dict of special laws (default is None)
    :type special_laws_dict: dict, optional
    :param special_laws_re: regex of special laws (default is None)
    :type special_laws_re: str, optional
    :param is_preprocessed: if the text is already preprocessed (default is True)
    :type is_preprocessed: bool, optional
    :param kwargs: is_text_preprocessed -- Condition if text is already preprocessed (default True), so special laws will as well.
    :rtype: str
    :return: The text with the special laws tokens created will be returned
    """

    return transform_syn(
        text,
        synonyms_list=special_laws_list,
        synonyms_dict=special_laws_dict,
        synonyms_re=special_laws_re,
        is_preprocessed=is_preprocessed,
        special_laws=True,
    )

def transform_unify_legislation(text, **kwargs):
    """
    Unify legislation tokens.\n
    Basically, unify laws and articles from their tokens: ST_LEG_LEI_lawn°_ARTIGO_artn°

    :param text: Text which will have its tokens unified
    :type text: str
    :rtype: str
    :return: Text with the tokens unified

    *Note: Only works if remove_stop_words step is executed before*
    """

    # ST_CAPUT
    caput = f"{ST_PREFIX}CAPUT"
    
    # ST_PARAGRAFO_digitoUNICO (UNICO "opcional")
    paragraph = fr"{ST_PREFIX}PARAGRAFO_(\d+|UNICO)"
    
    # ST_INCISO_[MDCLXVI]+ (qualquer um de MDCLXVI uma vez ou mais)
    item = fr"{ST_PREFIX}INCISO_[MDCLXVI]+"
    
    # ST_ALINEA_caracter+
    indent = fr"{ST_PREFIX}ALINEA_\w+"
    
    # ST_LEG_letraOUnumero+
    previous_token = "ST_LEG_[\w+|\d+]+"
    
    ignored_expression = (
        "e?\d?seguintes",
        "e?\d?subsequentes",
        paragraph,
        item,
        indent,
        caput,
        previous_token,
    )
    ignored_expression = "|".join(ignored_expression)
    
    prefix_result = "LEG_"
    
    # st_artigo_digito+UMcaracter(opcional)
    artigo = "(?P<artigo>st_artigo_\d+\w?)"
    
    # st_lei_|st_decreto_|st_emenda_ digito+|caracteres
    lei = "(?P<lei>(?:st_lei_|st_decreto_|st_emenda_)(?:\d+|\w+))"

    def build_token(m):
        result = (
            ST_PREFIX
            + prefix_result
            + m.group("lei").replace("ST_", "")
            + "_"
            + m.group("artigo").replace("ST_", "")
            + " "
            + m.group("ignored_expression")
            + m.group("lei")
        )
        return result

    # Flags=re.I (ignora case)
    # artigo(whitespace)+ignored_expression(whitespace)+lei
    # Obs: +, uma ou mais vezes / *, nenhum ou mais vezes
    regex_unify = re.compile(
        fr"{artigo}\s+(?P<ignored_expression>(?:(?:{ignored_expression})\s+)*){lei}",
        flags=re.I,
    )
    while regex_unify.findall(text) != []:
        text = regex_unify.sub(build_token, text)

    def build_token_2(m):
        result = (
            m.group("lei")
            + " "
            + m.group("ignored_expression")
            + ST_PREFIX
            + prefix_result
            + m.group("lei").replace("ST_", "")
            + "_"
            + m.group("artigo").replace("ST_", "")
        )
        return result

    regex_unify_2 = re.compile(
        fr"{lei}\s+(?P<ignored_expression>(?:(?:{ignored_expression})\s+)*){artigo}",
        flags=re.I,
    )
    while regex_unify_2.findall(text) != []:
        text = regex_unify_2.sub(build_token_2, text)

    return text

def transform_extract_articles(text, **kwargs):
    """
    Extract the article from a legislation token (ST_LEG_LEI_law_ARTIGO_art)

    :param text: Text which will have its articles extracted from the token
    :type text: str
    :rtype: str
    :return: Text with the articles extracted
    """

    # ST_LEG_LEI_char+_ARTIGO_char+
    # +, uma ou mais vezes
    legislation_re = (
        fr"(?P<legislation>{ST_PREFIX}LEG_LEI_\w+_(?P<article>ARTIGO_\w+))"
    )

    text = re.sub(
        legislation_re,
        lambda m: (
            ST_PREFIX + m.group("article") + " " + m.group("legislation")
        ),
        text,
        flags=re.I,
    )

    return text

def transform_precedent(text, **kwargs):
    """
    Based on a list of precedents, create custom tokens for its appearances in a text.\n
    A .txt file with the list is necessary.

    :param text: Text which will have its precedents tokens created
    :type text: str
    :rtype: str
    :return: Text with the precedents tokens
    """
        
    with open("precedents.txt") as f:
        precedents_list = list(map(str.strip, f.readlines()))

    precedents_dict = get_precedents_dict(precedents_list)
    precedents_regex = get_precedents_regex(precedents_list)

    def get_precedent(type_precedent, number, court=""):
        precedent_acronym = precedents_dict[type_precedent.strip()]
        precedent_acronym = precedent_acronym.format(
            court=(court.upper() + "_" if court else "")
        )
        precedent = f"{precedent_acronym}_{number}"
        length_padding = 2

        return precedent.center(len(precedent) + length_padding)

    def transform(m):
        precedent = m.group("precedent")
        number = m.group("number").replace("/", "").replace(".", "")
        number = number.replace("-", "").replace(",", "")
        court = m.group("court") or ""

        try:
            return get_precedent(
                precedent.lower().replace("\n", " "), number, court
            )
        except KeyError:
            logging.warn("Unknown precedent: '%s'", precedent)
            return m.string

    return re.sub(
        fr"{BOUNDARY_BEG}{precedents_regex}{BOUNDARY_END}",
        transform,
        text,
        flags=re.I,
    )

def transform_urls(text, **kwargs):
    """
    Create custom tokens for urls appearances in a text\n
    There are differents compositions of URLs.

    :param text: Text which will have its urls tokens created
    :type text: str
    :rtype: str
    :return: Text with the urls tokens created

    *Notes: May be helpful:* https://tools.ietf.org/html/rfc3986#section-3 \n
    *Only IPv4 supported*\n
    *Not good enough to validate an URL*
    """
    
    # 1-3d.1-3d (3x) exemplo=> 33.3.333.33
    ip = r"(\d{1,3}(\.\d{1,3}){3})"
    
    # letrascaracteres:// (tem que ter o ://)
    scheme = r"([a-zA-Z][\w+\-.]*://)"
    
    # www. (opcional) texto1-63.caracteres1-63 
    hostname = r"((www\.)?[\w\-]{1,63}(\.[\w\-~]{1,63})+)"

    common_tld = r"(com|br|io|org|net|edu|net|gov|blog|biz)"
    hostname_common = hostname[: -len("+)")] + fr"*\.{common_tld})"

    # :1-5d
    port = r"(:\d{1,5})"
    
    # /tudo (acaba quando tem whitespace)
    end = r"(/\S*)"

    pats = [
        fr"{scheme}({hostname}|{ip}){port}?{end}?", # Tipo 1 Url
        fr"{ip}{port}{end}?", # Tipo 2 Url
        fr"{hostname_common}{port}?{end}?", # Tipo 3 Url
    ]
    pat = r"|".join(pats)

    return re.sub(pat, f"{ST_PREFIX}URL", text, flags=re.I)