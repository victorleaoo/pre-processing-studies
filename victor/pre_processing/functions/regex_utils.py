INCIDENTS = r"(MC|AGR)"

STATES = r"""(AC|AL|AP|AM|BA|CE|DF|ES|GO|MA|MT|MS
            |MG|PA|PB|PR|PE|PI|RJ|RN|RS|RO|RR|SC|SP|SE|TO)"""

FEDERAL_COURTS = r"(?P<court>STF|STJ|AgR)"

PT_CHARS = r"úüÚÜíÍóÓôÔáÁéÉêÊãÃõÕàÀâÂçÇa-zA-Z"

# Can admit float number from any region and left zeros
NUMBERS = r"\b([0-9]+(?:[\.,][0-9]+)*)\b"

# Word boundaries optimized to porguese
_boundary_chars = fr"{PT_CHARS}\-_"
BOUNDARY_BEG = fr"(?<![{_boundary_chars}])"
BOUNDARY_END = fr"(?![{_boundary_chars}])"
