# Imports necessários
import re

# Variáveis globais usadas
ST_PREFIX = "ST_" # Special Token
PT_CHARS = r"úüÚÜíÍóÓôÔáÁéÉêÊãÃõÕàÀâÂçÇa-zA-Z"
# Word boundaries optimized to porguese
_boundary_chars = fr"{PT_CHARS}\-_"
BOUNDARY_BEG = fr"(?<![{_boundary_chars}])"
BOUNDARY_END = fr"(?![{_boundary_chars}])"

# Usada em:
# 1. Transform_decrees, 2. Transform_amendments, 3. Transform_articles
def _transform_legislations(
    text,
    prefix_result,
    re_legislation,
    is_multiple=False,
    identification=None,
    previous_token=None,
    separator=None,
):
    "Create custom tokens for legislation"

    def build_token(m):
        if m.group("ignored_expression"):
            result = m.group("ignored_expression")
        else:
            result = ""
        result = (
            result
            + ST_PREFIX
            + prefix_result
            + m.group("identification")
            .replace('"', "")
            .replace("“", "")
            .replace("”", "")
            .replace(".", "")
            .replace(",", "")
            .replace("º", "")
            .lstrip("0")
            .upper()
        )
        return result

    def build_token_multi(m):
        result = (
            m.group("ignored_expression")
            + ST_PREFIX
            + prefix_result
            + m.group("identification")
            .replace('"', "")
            .replace("“", "")
            .replace("”", "")
            .replace(".", "")
            .replace(",", "")
            .replace("º", "")
            .lstrip("0")
            .upper()
        )
        return result

    # replaces first occurrences
    text = re_legislation.sub(build_token, text)

    # replaces multiple occurrences, if exists
    if is_multiple:
        regex_legislation_multi = re.compile(
            fr"(?:(?P<ignored_expression>{previous_token}{separator}){identification})",
            flags=re.I,
        )
        while regex_legislation_multi.findall(text) != []:
            text = regex_legislation_multi.sub(build_token_multi, text)

    return text

# Usadas em 3. Transform_articles
def _transform_article_items(text, **kwargs):
    # Romanos
    roman_number = r"\b[MDCLXVI]+\b"
    
    # ST_CAPUT
    caput = f"{ST_PREFIX}CAPUT"
    
    prefix_result = "INCISO_"
    
    # ST_INCISO_romanos+
    previous_token = fr"{ST_PREFIX}{prefix_result}[MDCLXVI]+"
    
    # (whitespace)*,|e(whitespace)+
    separator = r"\s*[,e]\s+"
    
    # roman_number
    item = fr"(?P<identification>{roman_number})"
    
    # ST_ARTIGO_digito+,(opcional)(whitespace)+ST_CAPUT,(opcional)(whitespace)+roman_uber
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}ARTIGO_\d+(?:,?\s+{caput})?,?\s+){item})",
        flags=re.I,
    )
    
    # Exemplo para ser utilizado abaixo: ST_ARTIGO_188, ST_CAPUT, XV

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        item,
        previous_token,
        separator,
    )
def _transform_article_indents(text, **kwargs):
    """
    Create custom tokens for indents contained in a article
    and not explicitly mentioned in the text
    """

    prefix_result = "ALINEA_"
    
    # ST_CAPUT
    caput = f"{ST_PREFIX}CAPUT"
    
    # ST_ALINEA_caracter
    previous_token = fr"{ST_PREFIX}{prefix_result}\w+"
    
    # (whitespace)*,|e(whitespace)+
    separator = r"\s*[,e]\s+"
    
    # Não entendi muito bem, mas aparentemente qualquer caracterer
    indent = (
        r"(?<!\w)[\"|\“|\”]?(?P<identification>[a|b]?[a-z])[\"|\“|\”]?(?!\w)"
    )
    
    #ST_ARTIGO_digito+,(opcional)(whitespace)+ST_CAPUT,(whitespace)+indent
    # +, uma ou mais vezes
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}ARTIGO_\d+(?:,?\s+{caput})?,\s+){indent})",
        flags=re.I,
    )
    
    # Exemplo para usar mais abaixo: ST_ARTIGO_999, ST_CAPUT, i

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        indent,
        previous_token,
        separator,
    )
def transform_item_indents(text, **kwargs):
    """
    Create custom tokens for indents contained in a item
    and not explicitly mentioned in the text
    """

    prefix_result = "ALINEA_"
    
    # ST_ALINEA_char+
    previous_token = fr"{ST_PREFIX}{prefix_result}\w+"
    
    # (whitespace)*,|e(whitespace)+
    separator = r"\s*[,e]\s+"
    
    # Indent: char?
    indent = (
        r"(?<!\w)[\"|\“|\”]?(?P<identification>[a|b]?[a-z])[\"|\“|\”]?(?!\w)"
    )
    
    # ST_INCISO_romanos+,(whitespace)+indent
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}INCISO_[MDCLXVI]+,\s+){indent})",
        flags=re.I,
    )

    # Exemplo para usar abaixo: ST_INCISO_VI, k
    
    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        indent,
        previous_token,
        separator,
    )

# Usadas em  5. Transform_syn
def remove_extra_spaces(text, **kwargs):
    """Remove multiple spaces"""

    return re.sub(r"\s+", " ", text, flags=re.I).strip()
def preprocess_synonym(synonym):
    synonym = synonym.strip().lower()
    synonym = remove_extra_spaces(synonym)
    # synonym = replace_diacritic(synonym)

    return synonym
def _synonym_to_st(token, special_laws=False):
    if not token:
        raise RuntimeError("Empty token to be used as ST for synonym")

    token = token.replace(" ", "_")
    if special_laws:
        special_token = f"{ST_PREFIX}LEI_{token}".upper()
    else:
        special_token = f"{ST_PREFIX}SYN_{token}".upper()

    return special_token
def get_synonyms_re(
    synonyms_list=None,
    synonyms_dict=None,
    is_preprocessed=True,
    special_laws=False,
):
    """Return compiled regex for synonyms.
    For optimization, too big to compile everytime
    """

    if synonyms_dict is None:
        synonyms_dict = get_synonyms_dict(
            synonyms_list,
            is_preprocessed=is_preprocessed,
            special_laws=special_laws,
        )

    synonyms = list(synonyms_dict.keys())
    synonyms.sort(reverse=True, key=len)

    synonyms = "|".join(synonyms)
    synonyms = synonyms.replace("(", "\\(")
    synonyms = synonyms.replace(")", "\\)")
    synonyms = synonyms.replace(" ", "\s")

    return re.compile(fr"{BOUNDARY_BEG}({synonyms}){BOUNDARY_END}", flags=re.I)
# Basicamente pega a special laws ou synonyms e transforma em dicionário de ST_LEI_abreviacao
def get_synonyms_dict(
    synonyms_list=None,
    preprocess_fn=preprocess_synonym,
    is_preprocessed=True,
    special_laws=False,
):
    """
    Transforms an synonyms list to a synonyms dict,
    creating STs as necessary.

    Synonyms list format: ['syn10', 'syn11', '', 'syn21', 'syn22', ...].
    Each group of synonyms is separated by an empty string.
    The first member of each group will be used as ST.

    Synonyms dict format: synonyms_dict[token] = group_st
    group_st is the st for the current group
    token is token which has a synonym

    if synonyms_list is not given, it tries to read from a static file
    """
    preprocess_fn = str.strip if not is_preprocessed else preprocess_fn

    if special_laws:
        if synonyms_list is None:
            with open("special_laws.txt") as f:
                synonyms_list = map(preprocess_fn, f.readlines())
    else:
        if synonyms_list is None:
            with open("synonyms.txt") as f:
                synonyms_list = map(preprocess_fn, f.readlines())

    lines = iter(synonyms_list)
    synonyms_dict = dict()
    update_st_next = True

    for line in lines:
        if update_st_next:
            st = _synonym_to_st(line, special_laws)
            update_st_next = False

        if line:
            synonyms_dict[line.lower()] = st
        else:
            update_st_next = True

    return synonyms_dict

# Usadas em 9. Transform_precendents
# Dicionário: recurso especial: ST_STJ_RESP (exemplo)
def get_precedents_dict(precedents_list=None):
    """
    Transforms a precedents list into a precedents dict
    creating STs as necessary.

    Precedents list format: ['pcdt10', 'pcdt11', '', 'pcdt21', 'pcdt22', ...].
    Each group of precedents is separated by an empty string.
    the first member of each group will be used as ST.

    Precedents dict format: precedents_dict[token] = group_st
    group_st is the st for the current group
    token is the token which will converge to a precedent
    """
    precedents_dict = dict()
    update_st_next = True

    if precedents_list is None:
        with open("precedents.txt") as f:
            precedents_list = list(map(str.strip, f.readlines()))

    for index, line in enumerate(precedents_list):
        if update_st_next:
            acronym, court = line.split("|")
            precedents_list[index] = acronym
            if court:
                precedent_st = f"{ST_PREFIX}{court.upper()}_{acronym.upper()}"
            else:
                precedent_st = "{ST_PREFIX}{{court}}{acronym}".format(
                    ST_PREFIX=ST_PREFIX, acronym=acronym.upper()
                )
            precedents_dict[acronym.lower()] = precedent_st
            update_st_next = False
        elif line:
            precedents_dict[line.lower()] = precedent_st
        else:
            update_st_next = True

    return precedents_dict

# regex_utils
INCIDENTS = r"(MC|AGR)"

STATES = r"""(AC|AL|AP|AM|BA|CE|DF|ES|GO|MA|MT|MS
            |MG|PA|PB|PR|PE|PI|RJ|RN|RS|RO|RR|SC|SP|SE|TO)"""

FEDERAL_COURTS = r"(?P<court>STF|STJ|AgR)"

def get_precedents_regex(precedents_list=None):
    word_number = r"(?:número[s]?|n°s|ns\.|n\.o|n\.º|n2|no|nº|n\.|n)?"
    incident = fr"(\-{INCIDENTS})*"
    state = fr"(?:\s*(?:\w{{2}}|\\|\/|\-)\s*({STATES}|{FEDERAL_COURTS}))?"
    """
    Due to the following case found "AC 006003188.2008.4.01,9199/MG." where
    a comma exists between the numbers, the result of some cases
    like "(RESP 201700158919," will remove the comma at the end.
    """
    # numbers = r"(?P<number>[0-9\.\-,\/]+)"
    numbers = r"(?P<number>[0-9][0-9\.\-,\/]*[0-9]|[0-9])"

    if precedents_list is None:
        with open("precedents.txt") as f:
            precedents_list = list(map(str.strip, f.readlines()))

    precedents_list = filter(len, precedents_list)
    precedents_sorted = sorted(set(precedents_list), key=len, reverse=True)
    precedents_sorted = map(
        lambda x: r"\s+".join(x.split())
        .replace("(", "\\(")
        .replace(")", "\\)"),
        precedents_sorted,
    )
    precedents = r"|".join(precedents_sorted)
    precedents.replace("(", "\\(").replace(")", "\\)")
    acronym_group = fr"(?P<precedent>({precedents}))"
    precedents_regex = (
        fr"({acronym_group}\s*{word_number}\s*{numbers}{incident}{state})"
    )

    return precedents_regex