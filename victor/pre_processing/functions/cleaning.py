import logging
import re
import string

import nltk

from .regex_utils import PT_CHARS, BOUNDARY_BEG, BOUNDARY_END
from ..utils import get_stopwords, get_stemmer, get_spacy_nlp
from ..special_tokens import is_specialtoken, ST_PREFIX


def to_lower(text, **kwargs):
    """Lower all characters but not special tokens"""
    return re.sub(
        fr"{BOUNDARY_BEG}([{PT_CHARS}\_\d\-]+){BOUNDARY_END}",
        lambda m: m.group(1).lower()
        if not is_specialtoken(m.group(1))
        else m.group(1),
        text,
    )


def replace_diacritic(text, **kwargs):
    """Replace diacritic from chars. Ex: á -> a"""

    replacements = (
        ("ãáàâ", "a"),
        ("ÃÁÀÂ", "A"),
        ("éê", "e"),
        ("ÉÊ", "E"),
        ("í", "i"),
        ("ı", "i"),  # OCR things
        ("Í", "I"),
        ("óõô", "o"),
        ("ÓÕÔ", "O"),
        ("ú", "u"),
        ("Ú", "U"),
        ("ç", "c"),
        ("Ç", "C"),
    )

    for pat, sub in replacements:
        text = re.sub(fr"[{pat}]", sub, text)

    return text


def remove_numbers(text, **kwargs):
    """Removes numbers from text"""

    return re.sub(r"\b\d[\d,.]*\b", "", text)


def remove_small_big_words(text, **kwargs):
    """Remove words bigger than 30 or smaller than 3 characters"""

    def is_small_big_word(m):
        w = m.group(1)
        return (
            "" if (len(w) < 3 or len(w) > 30) and not is_specialtoken(w) else w
        )

    return re.sub(r"([^\s]+)", is_small_big_word, text, flags=re.I)


# Give spellcheker a chance first
def remove_letter_numbers(text, **kwargs):
    """
    Remove any word with numbers but keeps special tokens like: LEI_13105
    """

    # Remove wor00 00wo 00wor00 wo00rd and keep WORD_000
    return re.sub(r"([A-Z]+_\d+)|[^ ]*\d+[^ ]*", r"\1", text)


def remove_non_pt_chars(text, **kwargs):
    """
    Removes words that characters that doesn't seem to be portuguese
    words or special tokens left on purpose, like: ST_LEI_8112
    """

    return re.sub(
        fr"[^\s\d_{PT_CHARS}{string.punctuation}]", "", text, flags=re.I
    )


def remove_punctuations(text, embedding=False, **kwargs):
    """
    Removes punctuations at the word and outside.
    """

    punctuation = string.punctuation

    if embedding:  # TODO: write test for this
        punctuation = re.sub(r"[;.?!]", "", punctuation)

    st = fr"(?:{ST_PREFIX}[A-Z_\d]+)"
    composed = fr"(?:[{PT_CHARS}]+(?:-[{PT_CHARS}]+)+)"
    punctuation = fr"(?P<punctuation>[{punctuation}]+)"
    number = r"(?:-?\d[\d,.]+)"

    return re.sub(
        fr"(?P<keep>{st}|{number}|{composed})|{punctuation}",
        lambda m: " "  # Space to ensure tokens will not get joined
        if m.group("punctuation")
        else m.group("keep"),
        text,
    )


def remove_extra_spaces(text, **kwargs):
    """Remove multiple spaces"""

    return re.sub(r"\s+", " ", text, flags=re.I).strip()


def remove_stop_words(text, stop_words=(), extra_stop_words=(), **kwargs):
    """Remove the stop words, ignoring case"""

    stop_words = get_stopwords(stop_words, extra_stop_words)

    def is_stopword(m):
        w = m.group(1)
        return "" if w.lower() in stop_words else w

    return re.sub(
        fr"\b{BOUNDARY_BEG}([{PT_CHARS}]+){BOUNDARY_END}\b",
        is_stopword,
        text,
        flags=re.I,
    )


def remove_words_one_letter_replications(text, **kwargs):
    """
    Remove words that are composed by repetitions of the same
    character. Ex: aa bbb ççç
    """
    return re.sub(
        fr"{BOUNDARY_BEG}(?:([{PT_CHARS}])\1+){BOUNDARY_END}",
        "",
        text,
        flags=re.I,
    )


def remove_leading_cedillas(text, **kwargs):
    """
    Remove leading cedillas which are confused with § symbol
    """

    return re.sub(r"\bçç(?<!\s)", "", text, flags=re.I)


def tokenize(text, **kwargs):
    """
    Transform string texts in array of tokens.
    The default behavior is split in spaces
    """

    return nltk.word_tokenize(text)


def lemmatize(text, enforce_lower=True, nlp=None, **kwargs):
    """
    Lemmatize the words from a text.
    Diacritic makes difference.
    The tokens that will be transform_ed must be lower case.
    After the application, tokens will be separated by spaces and lower.
    """

    if enforce_lower:
        text = to_lower(text)

    if nlp is None:
        nlp = get_spacy_nlp()

    try:
        parsed = nlp(text)
    except ValueError:  # ignore step if len of string is greater then 2**30
        logging.error("Length of text is greater then 2**30")
        return text

    return " ".join(
        t.lemma_ if not is_specialtoken(t.text) else t.text for t in parsed
    )


def stem(tokens, stemmer=None, **kwargs):
    """
    Use nltk Snowball Stemmer to stemmize words
    """

    if stemmer is None:
        stemmer = get_stemmer()

    return [
        stemmer.stem(word) if not is_specialtoken(word) else word
        for word in tokens
    ]
