# Pré-processamento de Dados

Repositório para estudo de pré-processamento de dados de texto e realização de tarefas que convém à GPAM e Preprocessing no geral. Os conhecimentos aqui descritos foram obtidos a partir do **TDD** realizado, principalmente, pelo **professor Thiago de Paulo no AiLab** e a partir do **livro "Text Analytics with Python"**.

## Estudo de Pré-processamento

O arquivo [study.md](https://gitlab.com/victorleaoo/pre-processing-studies/-/blob/main/study.md) possui um resumo de tudo que foi estudado até agora acerca de pré-processamento de textos.

Também foi criado um [notebook com exemplos](https://gitlab.com/victorleaoo/pre-processing-studies/-/blob/main/preprocessing_study.ipynb).

## GPAM

Para as funções da GPAM, a pasta [functions_GPAM](https://gitlab.com/victorleaoo/pre-processing-studies/-/tree/main/functions_GPAM) possui o [notebook](https://gitlab.com/victorleaoo/pre-processing-studies/-/blob/main/functions_GPAM/gpam_functions.ipynb) com as funções designadas documentadas e com exemplos.

Além disso, dentro dessa mesma pasta da GPAM, há a pasta [docs](https://gitlab.com/victorleaoo/pre-processing-studies/-/tree/main/functions_GPAM/docs/docs) com a documentação seguindo o **Sphinx**. Mais especificamente, na pasta [source](https://gitlab.com/victorleaoo/pre-processing-studies/-/tree/main/functions_GPAM/docs/docs/source), há os arquivos .rst responsáveis pela execução da auto documentação das funções presentes na gpam.py (mesmas do notebook citadas).

Para acessar a página, é necessário baixar os arquivos e acessar os arquivos .html presentes em [docs/build/html](https://gitlab.com/victorleaoo/pre-processing-studies/-/tree/main/functions_GPAM/docs/docs/build/html). *Interessante acessar o index.html, já que é a página inicial.*