.. GPAM documentation master file, created by
   sphinx-quickstart on Tue May 17 11:41:21 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GPAM's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
