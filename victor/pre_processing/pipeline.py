import sys
from functools import partial
import multiprocessing as mp
from timeit import default_timer as timer
from typing import Tuple

from tqdm import tqdm
from cached_property import cached_property

from .functions.cleaning import (
    lemmatize,
    remove_extra_spaces,
    remove_leading_cedillas,
    remove_non_pt_chars,
    remove_punctuations,
    remove_small_big_words,
    remove_stop_words,
    remove_words_one_letter_replications,
    remove_numbers,
    replace_diacritic,
    to_lower,
    tokenize,
)
from .functions.transformation import (
    correct_ocr_errors,
    expand_abreviations,
    transform_caput,
    transform_paragraphs,
    transform_items,
    transform_paragraph_items,
    transform_indents,
    transform_paragraph_indents,
    transform_item_indents,
    transform_laws,
    transform_decrees,
    transform_amendments,
    transform_articles,
    transform_emails,
    transform_syn,
    transform_special_laws,
    transform_unify_legislation,
    transform_extract_articles,
    transform_precedent,
    transform_urls,
    transform_upper,
    transform_firstupper,
    transform_char_rep,
    transform_token_rep,
    transform_text_begin,
    get_known_words,
    get_synonyms_re,
    get_synonyms_dict,
)
from .spellchecker import spellcheck
from .utils import get_stemmer, get_spacy_nlp, get_vocabulary

INCLUDES_NER = False

try:
    from pre_processing_ner.ner import transform_ner

    INCLUDES_NER = True
except Exception:
    INCLUDES_NER = False

    def transform_ner():
        """
        Generic function to represent the ner import
        """
        pass


class Pipeline:
    # Default behavior for preprocessing pipeline
    # In order of execution
    default_steps = (
        remove_non_pt_chars,
        correct_ocr_errors,
        expand_abreviations,
        transform_special_laws,
        transform_syn,
        transform_text_begin,
        transform_caput,
        transform_paragraphs,
        transform_items,
        transform_paragraph_items,
        transform_indents,
        transform_paragraph_indents,
        transform_item_indents,
        transform_laws,
        transform_decrees,
        transform_amendments,
        transform_articles,
        transform_emails,
        transform_precedent,
        transform_urls,
        transform_ner,  # after(laws, articles, decrees, urls)
        remove_words_one_letter_replications,
        remove_leading_cedillas,
        remove_numbers,  # after(transform_*)
        remove_punctuations,  # after(transform_*)
        remove_small_big_words,  # after(punctuations)
        remove_stop_words,  # before(diacritic,lemmatize)
        remove_extra_spaces,  # last remove_*
        spellcheck,  # before(stop_words,diacrict,lemmatize,small_big)
        transform_unify_legislation,  # after(transform_special_laws, remove_stop_words)
        transform_extract_articles,
        lemmatize,  # before(diacritic)
        replace_diacritic,
        # This block after all removes, dont mark what doesnt exists
        # syn better before transforms which add tokens but needs
        #   vocabulary file with infinitives to work well with lemmatize
        transform_token_rep,
        transform_upper,  # last to_st except by to_lower
        transform_char_rep,
        transform_firstupper,  # before(st_upper)
        to_lower,
        tokenize,  # last
    )

    _fixed_disabled_default = (
        "transform_text_begin",
        "transform_token_rep",
        "transform_char_rep",
        "transform_upper",
        "transform_firstupper",
        "transform_unknown",
    )

    _fixed_disabled_embedding = (
        "remove_words_one_letter_replications",
        "remove_numbers",
        "remove_stop_words",
        "remove_small_big_words",
        "transform_unify_legislation",
        "transform_extract_articles",
        "lemmatize",
    )

    # Useful for not loading heavy attributes when not neeeded
    # key: attribute name, value: set with functions with needs
    #   the attribute which name is on key
    _heavy_attributes = {
        "stemmer": frozenset(["stem"]),
        "spacy_nlp": frozenset(["lemmatize"]),
        "known_words": frozenset(["transform_unknown"]),
        "synonyms_re": frozenset(["transform_syn"]),
        "synonyms_dict": frozenset(["transform_syn"]),
    }

    def __init__(
        self,
        steps=default_steps,
        disabled_steps=("transform_ner", "spellcheck"),
        *,
        fixed_disabled_steps=None,
        embedding=True,
        **kwargs,
    ):
        if fixed_disabled_steps is None:
            fixed_disabled_steps = (
                self._fixed_disabled_embedding
                if embedding
                else self._fixed_disabled_default
            )

        disabled_steps += fixed_disabled_steps

        if "transform_ner" not in disabled_steps and (
            not INCLUDES_NER or not sys.modules.get("pre_processing_ner")
        ):

            error_msg = (
                "Ner module not included. Run pip install git+http://"
                "gitlab.com/gpam/services/pre-processing-ner.git@"
                "version_tag to install the module."
            )

            raise ImportError(error_msg)

        disabled_steps = set(
            d.__name__ if not isinstance(d, str) else d for d in disabled_steps
        )

        # Order matters
        self._active_names = [
            s.__name__ if not isinstance(s, str) else s for s in steps
        ]

        self._active_names = [
            s for s in self._active_names if s not in disabled_steps
        ]

        self._active = {
            n: getattr(sys.modules[__name__], n) for n in self._active_names
        }

        self.embedding = embedding

        # Functions set to None here will replace cached_properties
        #   avoiding loading them
        for attr, fns in self._heavy_attributes.items():
            if not any(fn in self._active_names for fn in fns):
                setattr(self, attr, None)

        # Attributes that can be used by any active function
        self._kwargs = {
            "embedding": self.embedding,
            "stemmer": self.stemmer,
            "nlp": self.spacy_nlp,
            "known_words": self.known_words,
            "synonyms_re": self.synonyms_re,
            "synonyms_dict": self.synonyms_dict,
            "is_preprocessed": True,  # So, preprocess synonyms
        }

    @cached_property
    def stemmer(self):
        return get_stemmer()

    @cached_property
    def spacy_nlp(self):
        return get_spacy_nlp()

    @cached_property
    def vocabulary(self):
        return get_vocabulary()

    @cached_property
    def known_words(self):
        return get_known_words()

    @cached_property
    def synonyms_dict(self):
        # transform_syn is one of the last functions to be called
        #   so, text will be preprocessed when getting to it
        return get_synonyms_dict(is_preprocessed=True)

    @cached_property
    def synonyms_re(self):
        return get_synonyms_re(
            synonyms_dict=self.synonyms_dict, is_preprocessed=True
        )

    def apply(self, text, *, verbose=False):
        """
        Pre-process the data applying a pipeline of functions

        Arguments:
            :text: the text to clear
            :disable: functions to disable on pipeline
            :verbose: print each called function
        """

        def print_verbose(s):
            if verbose:
                print(s)

        if not text:
            return ""

        for name in self._active_names:
            print_verbose(f"START: Applying '{name}'...")

            start = timer()
            step = self._active[name]
            text = step(text, **self._kwargs)
            end = timer()

            print_verbose(f"END: '{name}' applied in {end - start} s")

        return text

    def apply_batch(self, texts, *, num_cpus=mp.cpu_count()):
        def process(ts, pipeline, desc=""):
            partial_step = partial(self._apply_batch_step, pipeline)
            chunksize = int(max(1, (len(ts) / num_cpus) // 10))

            with mp.Pool() as pool:
                results = pool.imap_unordered(
                    partial_step, ts, chunksize=chunksize
                )

                results = list(
                    tqdm(results, desc=desc, unit="texts", total=len(ts))
                )

            return results

        self._kwargs["is_batch"] = True

        # Not tokenizing in batch
        try:
            self._active_names.remove("tokenize")
        except ValueError:
            ...

        if not texts:
            return list(tuple())

        if "spellcheck" in self._active_names:
            spellchecker_idx = self._active_names.index("spellcheck")

            pipeline_before = self._active_names[:spellchecker_idx]
            texts = process(
                texts, pipeline_before, "Running before spellchecker functions"
            )

            texts = spellcheck(texts, is_batch=True)

            pipeline_after = self._active_names[spellchecker_idx + 1:]
            texts = process(
                texts, pipeline_after, "Running after spellchecker functions"
            )
        else:
            texts = process(
                texts,
                self._active_names,
                "Running all preprocessing functions",
            )

        return texts

    def _apply_batch_step(self, steps_names, text=Tuple[int, str]):
        """
        Helper function to be submitted by a executor
        """
        idx, t = text
        for name in steps_names:
            step = self._active[name]
            t = step(t, **self._kwargs)

        return idx, t
