import logging
import re

from .regex_utils import (
    INCIDENTS,
    PT_CHARS,
    BOUNDARY_BEG,
    BOUNDARY_END,
    STATES,
    FEDERAL_COURTS,
)
from .cleaning import replace_diacritic, remove_extra_spaces
from ..special_tokens import ST_PREFIX, ST_PAT, is_specialtoken
from ..utils import (
    get_stopwords,
    get_vocabulary,
    NAMEDENTITIES_FILE,
    SYNONYMS_FILE,
    SPECIAL_LAWS_FILE,
    PRECEDENTS_PREFIX_FILE,
    OCR_ERRORS_FILE,
    ABREVIATIONS_FILE,
)


def transform_caput(text, **kwargs):
    """
    Create custom tokens for caput in a text

    :param text: Text which will have its caput tokens created
    :type text: str
    :rtype: str
    :return: The text will be returned with the caputs tokens created
    """

    re_caput = r"[\"|\“|\”]?caput[\"|\“|\”]?(?!\w)"

    return re.sub(re_caput, f"{ST_PREFIX}CAPUT", text, flags=re.I)


def _transform_legislations(
    text,
    prefix_result,
    re_legislation,
    is_multiple=False,
    identification=None,
    previous_token=None,
    separator=None,
):
    "Create custom tokens for legislation"

    def build_token(m):
        if m.group("ignored_expression"):
            result = m.group("ignored_expression")
        else:
            result = ""
        result = (
            result
            + ST_PREFIX
            + prefix_result
            + m.group("identification")
            .replace('"', "")
            .replace("“", "")
            .replace("”", "")
            .replace(".", "")
            .replace(",", "")
            .replace("º", "")
            .lstrip("0")
            .upper()
        )
        return result

    def build_token_multi(m):
        result = (
            m.group("ignored_expression")
            + ST_PREFIX
            + prefix_result
            + m.group("identification")
            .replace('"', "")
            .replace("“", "")
            .replace("”", "")
            .replace(".", "")
            .replace(",", "")
            .replace("º", "")
            .lstrip("0")
            .upper()
        )
        return result

    # replaces first occurrences
    text = re_legislation.sub(build_token, text)

    # replaces multiple occurrences, if exists
    if is_multiple:
        regex_legislation_multi = re.compile(
            fr"(?:(?P<ignored_expression>{previous_token}{separator}){identification})",
            flags=re.I,
        )
        while regex_legislation_multi.findall(text) != []:
            text = regex_legislation_multi.sub(build_token_multi, text)

    return text


def transform_paragraphs(text, **kwargs):
    """
    Create custom tokens for paragraphs appearances in a text

    :param text: Text which will have its paragraphs tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the paragraphs tokens created
    """

    single_paragraph_re = (
        r"par[aá]grafo\s+[\"|\“|\”]?[uú]nico[\"|\“|\”]?(?!\w)"
    )

    text = re.sub(
        single_paragraph_re, f"{ST_PREFIX}PARAGRAFO_UNICO", text, flags=re.I
    )

    number = r"(?:\d+(?:[\.,]\d+)*)"
    prefix_result = "PARAGRAFO_"
    paragraph_marks = r"(?:[58]\s|[&\$§])"
    paragraph = fr"(?P<identification>{number}º?)"
    re_legislation = re.compile(
        fr"(?<!\w)(?P<ignored_expression>)(?:{paragraph_marks}\s*{paragraph})",
        flags=re.I,
    )

    return _transform_legislations(text, prefix_result, re_legislation, False)


def transform_items(text, **kwargs):
    """
    Create custom tokens for items

    :param text: Text which will have its items tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the items tokens created
    """

    roman_number = r"\b[MDCLXVI]+\b"
    prefix_result = "INCISO_"
    previous_token = fr"{ST_PREFIX}{prefix_result}[MDCLXVI]+"
    separator = r"\s*[,e]\s+"
    item = fr"(?P<identification>{roman_number})"
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(?:incisos?\s+{item})", flags=re.I
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        item,
        previous_token,
        separator,
    )


def transform_paragraph_items(text, **kwargs):
    """
    Create custom tokens for items contained in a paragraph
    and not explicitly mentioned in the text

    :param text: Text which will have its items tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the items tokens created
    """

    roman_number = r"\b[MDCLXVI]+\b"
    prefix_result = "INCISO_"
    previous_token = fr"{ST_PREFIX}{prefix_result}[MDCLXVI]+"
    separator = r"\s*[,e]\s+"
    item = fr"(?P<identification>{roman_number})"
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}PARAGRAFO_(\d+|UNICO),\s+){item})",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        item,
        previous_token,
        separator,
    )


def transform_indents(text, **kwargs):
    """
    Create custom tokens for indents

    :param text: Text which will have its indents tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the indents tokens created
    """

    prefix_result = "ALINEA_"
    previous_token = fr"{ST_PREFIX}{prefix_result}\w+"
    separator = r"\s*[,e]\s+"
    indent = (
        r"(?<!\w)[\"|\“|\”]?(?P<identification>[a|b]?[a-z])[\"|\“|\”]?(?!\w)"
    )
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(?:al[í|i]neas?\s+{indent})", flags=re.I
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        indent,
        previous_token,
        separator,
    )


def transform_paragraph_indents(text, **kwargs):
    """
    Create custom tokens for indents contained in a paragraph
    and not explicitly mentioned in the text

    :param text: Text which will have its indents tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the indents tokens created
    """

    prefix_result = "ALINEA_"
    previous_token = fr"{ST_PREFIX}{prefix_result}\w+"
    separator = r"\s*[,e]\s+"
    indent = (
        r"(?<!\w)[\"|\“|\”]?(?P<identification>[a|b]?[a-z])[\"|\“|\”]?(?!\w)"
    )
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}PARAGRAFO_(\d+|UNICO),\s+){indent})",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        indent,
        previous_token,
        separator,
    )


def transform_item_indents(text, **kwargs):
    """
    Create custom tokens for indents contained in a item
    and not explicitly mentioned in the text

    :param text: Text which will have its indents tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the indents tokens created
    """

    prefix_result = "ALINEA_"
    previous_token = fr"{ST_PREFIX}{prefix_result}\w+"
    separator = r"\s*[,e]\s+"
    indent = (
        r"(?<!\w)[\"|\“|\”]?(?P<identification>[a|b]?[a-z])[\"|\“|\”]?(?!\w)"
    )
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}INCISO_[MDCLXVI]+,\s+){indent})",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        indent,
        previous_token,
        separator,
    )


def transform_laws(text, **kwargs):
    """
    Create custom tokens for laws

    :param text: Text which will have its laws tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the laws tokens created
    """

    law_types = (
        "complementar(?:es)?",
        "federa(?:l|is)",
        "estadua(?:l|is)",
        "municipa(?:l|is)",
    )
    law_types = "|".join(law_types)

    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"
    number = r"(?:\d+(?:[\.,]\d+)*)"
    year = fr"(?:[/-]{number})?"
    prefix_result = "LEI_"
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    separator = r"\s*[,e]\s+"
    law = fr"(?P<identification>{number}){year}"
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(?<!\w)(leis?|l\.)(\s+({law_types}))?\s+(?:{word_number}\s*)?{law}",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        law,
        previous_token,
        separator,
    )


def transform_decrees(text, **kwargs):
    """
    Create custom tokens for decrees appearances in a text

    :param text: Text which will have its decrees tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the decrees tokens created
    """

    decree_types = (
        "leis?",
        "regulamentador(?:es)?",
        "presidencia(?:l|is)",
        "federa(?:l|is)",
        "estadua(?:l|is)",
        "executivos?",
        "leis? complementar(?:es)?",
        "municipa(?:l|is)",
        "legislativos?",
        "legislativos? estadua(?:l|is)",
        "legislativos? municipa(?:l|is)",
    )
    decree_types = "|".join(decree_types)

    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"
    number = r"(?:\d+(?:[\.,]\d+)*)"
    year = fr"(?:[/-]{number})?"
    prefix_result = "DECRETO_"
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    separator = r"\s*[,e]\s+"
    decree = fr"(?P<identification>{number}){year}"
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(decretos?([\s-]+({decree_types}))?)\s+(?:{word_number}\s*)?{decree}",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        decree,
        previous_token,
        separator,
    )


def transform_amendments(text, **kwargs):
    """
    Create custom tokens for amendments appearances in a text

    :param text: Text which will have its admendments tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the admendments tokens created
    """


    amendment_types = ("constituciona(?:l|is)", "regimenta(?:l|is)")
    amendment_types = "|".join(amendment_types)

    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"
    number = r"(?:\d+(?:[\.,]\d+)*)"
    year = fr"(?:[/-]{number})?"
    prefix_result = "EMENDA_"
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    separator = r"\s*[,e]\s+"
    amendment = fr"(?P<identification>{number}){year}"
    re_legislation = re.compile(
        fr"(?P<ignored_expression>)(emendas?(\s+({amendment_types}))?)\s+(?:{word_number}\s*)?{amendment}",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        amendment,
        previous_token,
        separator,
    )


def _transform_article_items(text, **kwargs):
    """
    Create custom tokens for items contained in a article
    and not explicitly mentioned in the text
    """

    roman_number = r"\b[MDCLXVI]+\b"
    caput = f"{ST_PREFIX}CAPUT"
    prefix_result = "INCISO_"
    previous_token = fr"{ST_PREFIX}{prefix_result}[MDCLXVI]+"
    separator = r"\s*[,e]\s+"
    item = fr"(?P<identification>{roman_number})"
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}ARTIGO_\d+(?:,?\s+{caput})?,?\s+){item})",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        item,
        previous_token,
        separator,
    )


def _transform_article_indents(text, **kwargs):
    """
    Create custom tokens for indents contained in a article
    and not explicitly mentioned in the text
    """

    prefix_result = "ALINEA_"
    caput = f"{ST_PREFIX}CAPUT"
    previous_token = fr"{ST_PREFIX}{prefix_result}\w+"
    separator = r"\s*[,e]\s+"
    indent = (
        r"(?<!\w)[\"|\“|\”]?(?P<identification>[a|b]?[a-z])[\"|\“|\”]?(?!\w)"
    )
    re_legislation = re.compile(
        fr"(?:(?P<ignored_expression>{ST_PREFIX}ARTIGO_\d+(?:,?\s+{caput})?,\s+){indent})",
        flags=re.I,
    )

    return _transform_legislations(
        text,
        prefix_result,
        re_legislation,
        True,
        indent,
        previous_token,
        separator,
    )


def transform_articles(text, **kwargs):
    """
    Create custom tokens for articles appearances in a text.\n
    It also works to create tokens for the articles items and indents

    :param text: Text which will have its articles tokens created
    :type text: str
    :rtype: str
    :return: From the function _transform_legislations (create custom tokens for legislation) the text will be returned with the articles tokens created
    """


    word_number = r"(?:n[oº]?\.?|n\.?º|n[úu]mero)"
    number = r"(?:\d+(?:[\.,]\d+)*)"
    prefix_result = "ARTIGO_"
    previous_token = fr"{ST_PREFIX}{prefix_result}{number}"
    separator = r"\s*[,e]\s+"
    article = fr"(?P<article>{number}º?(?:-?\w+)?)"
    re_article = re.compile(
        fr"(?:arts?\.?|artigos?)\s+(?:{word_number}\s*)?{article}", flags=re.I
    )

    def build_token(m):
        result = (
            ST_PREFIX
            + prefix_result
            + m.group("article")
            .replace("-", "")
            .replace("º", "")
            .replace(".", "")
            .replace(",", "")
            .lstrip("0")
            .upper()
        )
        return result

    # replaces the first article occurences
    text = re_article.sub(build_token, text)

    text = _transform_article_items(text)
    text = _transform_article_indents(text)
    text = transform_item_indents(text)

    def build_token_multi(m):
        result = (
            m.group("ignored_expression")
            + ST_PREFIX
            + prefix_result
            + m.group("article")
            .replace("-", "")
            .replace("º", "")
            .replace(".", "")
            .replace(",", "")
            .lstrip("0")
            .upper()
        )
        return result

    # transform multiple article occurences if exists
    caput = f"{ST_PREFIX}CAPUT"
    paragraph = fr"{ST_PREFIX}PARAGRAFO_(\d+|UNICO)"
    item = fr"{ST_PREFIX}INCISO_[MDCLXVI]+"
    indent = fr"{ST_PREFIX}ALINEA_\w+"
    ignored_expression = fr"{previous_token}(?:,?\s+{caput})?(\s|,|e|{paragraph}|{item}|{indent})*"
    regex_article_multi = re.compile(
        fr"(?:(?P<ignored_expression>{ignored_expression}{separator}){article})",
        flags=re.I,
    )
    while regex_article_multi.findall(text) != []:
        text = regex_article_multi.sub(build_token_multi, text)
        text = _transform_article_items(text)
        text = _transform_article_indents(text)
        text = transform_item_indents(text)

    return text


def transform_emails(text, **kwargs):
    """
    Create custom tokens for email appearances in a text.\n
    The composition of e-mail is local_part@domain.domain

    :param text: Text which will have its emails tokens created
    :type text: str
    :rtype: str
    :return: Text with the emails token created (from local_part@domain.domain to ST_EMAIL)

    *Note: Not good enough to validate an e-mail*
    """

    local_part = r"[\w!#$%&'*+\-/=?^_`{|}~.]+"
    domain = r"[a-zA-Z][\w\-]*(\.\w[\w\-]+)+"

    return re.sub(
        fr"{local_part}@{domain}", f"{ST_PREFIX}EMAIL", text, flags=re.I
    )


def transform_upper(text, **kwargs):
    """
    Search in the text for all the words that are completely in UPPER_CASE and transform them into LOWER_CASE.

    :param text: text that will be used for verification
    :type text: str
    :return: Text in lower case
    :rtype: str
    """
    pat_upper = fr"(?:[\-\d_]*[{PT_CHARS.upper()}][_\-\d]*)"
    pat_upper = fr"{BOUNDARY_BEG}(?P<upper>{pat_upper}+){BOUNDARY_END}"

    return re.sub(
        fr"(?P<st>{ST_PAT})|{pat_upper}",
        lambda m: (
            m.group("st") or f"{ST_PREFIX}UPPER {m.group('upper').lower()}"
        ),
        text,
    )


def transform_firstupper(text, **kwargs):
    """
	Search in the text for all words that start with a capital letter and convert the letter of that word to lowercase.

	:param text: text that will be used for verification
	:type text: str
	:return: Text in lower case
	:rtype: str
	"""
    upper = PT_CHARS.upper()
    lower = PT_CHARS.lower()

    pat = fr"(?:[{upper}][{lower}\-]*[_\-\d]*)"
    pat = fr"{BOUNDARY_BEG}(?P<token>{pat}){BOUNDARY_END}"

    return re.sub(
        fr"(?P<st>{ST_PAT})|{pat}",
        lambda m: (
            f"{ST_PREFIX}FIRST_UPPER {m.group('token').lower()}"
            if m.group("token") and m.group("token")[0].isupper()
            else m.group("token") or m.group("st")
        ),
        text,
    )


def transform_char_rep(text, **kwargs):
    """
	This function removes repeated characters when they are isolated (2 characters or more).

	* Ex1.: input (No dia seguinte...) | output (No dia seguinte...)
	* Ex2.: input (No diaaa aa seguinte...) | output (No diaaa ST_CHAR_REP 2 a seguinte...)
	* Ex2.: input (Noo diaaa aa seguinte 77 ...) | output (Noo diaaa ST_CHAR_REP 2 a seguinte ST_CHAR_REP 2 7 ...)

	:param text: text that will be used for verification
	:type text: str
	:return: Text specifying where the repeating character was found and the amount of that character
	:rtype: str
	"""
    def replace_rep(m):
        rep = None

        if m.group("word"):
            return m.group("word")

        num = m.group("num")
        if num is not None:
            if len(set(num)) == 1:
                # -1 to have size - 1 and be consistent with others
                rep = num[:-1]
            else:
                return num

        rep_word, rep_other = m.group("rep_word", "rep_other")
        rep = rep or rep_word or rep_other

        return f" {ST_PREFIX}CHAR_REP {len(rep)+1} {rep[0]}"

    number = r"(?P<num>-?\d[\d,.]+)"

    rep_word = fr"([{PT_CHARS}])(?P<rep_word>\2+)"
    rep_word = fr"(?:{BOUNDARY_BEG}{rep_word}{BOUNDARY_END})"

    word = fr"[{PT_CHARS}\-_]*[{PT_CHARS}][{PT_CHARS}\-_]*"
    word = fr"(?P<word>{BOUNDARY_BEG}{word}{BOUNDARY_END})"

    rep_other = fr"(?:([^{PT_CHARS}\s])(?P<rep_other>\5+))"

    return re.sub(
        fr"{number}|{rep_word}|{word}|{rep_other}",
        replace_rep,
        text,
        flags=re.I,
    )


def transform_text_begin(text, add_end=False, **kwargs):
    """
    This function indicates the beginning and the end (if assigned as True) of a text.

    :param text: text that will be used for verification
    :type text: str
    :param add_end: indicates whether the end-of-text marker will be displayed
    :type add_end: bool
    :return: returns text with start and end text markers
    :rtype: str
    """
    st_beg = f"{ST_PREFIX}TEXT_BEGIN "
    st_end = f" {ST_PREFIX}TEXT_END"

    end = ""
    if not text.endswith(st_end) and add_end:
        end = st_end

    start = ""
    if not text.startswith(st_beg):
        start = st_beg

    return f"{start}{text}{end}"


def transform_token_rep(text, **kwargs):
    """
    This function removes repeated words when they are in a sequence.

    * Ex1.: input (No dia seguinte...) | output (No dia seguinte...)
    * Ex1.: input (No dia dia seguinte...) | output (No ST_TOKEN_REP 2 dia seguinte...)
    * Ex1.: input (No dia e dia seguinte seguinte seguinte e seguinte...) | output (No dia ST_TOKEN_REP 3 seguinte e seguinte...)

    :param text: text that will be used for verification
    :type text: str
    :return: Text specifying where the repeating word (token) was found and the amount of that word (token)
    :rtype: str
    """
    boundary_beg = fr"(?<![{PT_CHARS}\-_\.,])"
    boundary_end = fr"(?![{PT_CHARS}\-_\.,])"
    pat = fr"{boundary_beg}([{PT_CHARS}\-_.\d]+){boundary_end}"

    def transform_(m):
        rep = m.group("rep")
        token = m.group("token")

        return f"{ST_PREFIX}TOKEN_REP {len(rep.split())+1} {token}"

    return re.sub(
        fr"(?P<token>{pat})(?P<rep>(?:\s\1)+){BOUNDARY_END}",
        transform_,
        text,
        re.I,
    )


def transform_urls(text, **kwargs):
    """
    Create custom tokens for urls appearances in a text\n
    There are differents compositions of URLs.

    :param text: Text which will have its urls tokens created
    :type text: str
    :rtype: str
    :return: Text with the urls tokens created

    *Notes: May be helpful:* https://tools.ietf.org/html/rfc3986#section-3 \n
    *Only IPv4 supported*\n
    *Not good enough to validate an URL*
    """

    ip = r"(\d{1,3}(\.\d{1,3}){3})"
    scheme = r"([a-zA-Z][\w+\-.]*://)"
    hostname = r"((www\.)?[\w\-]{1,63}(\.[\w\-~]{1,63})+)"

    common_tld = r"(com|br|io|org|net|edu|net|gov|blog|biz)"
    hostname_common = hostname[: -len("+)")] + fr"*\.{common_tld})"

    port = r"(:\d{1,5})"
    end = r"(/\S*)"

    pats = [
        fr"{scheme}({hostname}|{ip}){port}?{end}?",
        fr"{ip}{port}{end}?",
        fr"{hostname_common}{port}?{end}?",
    ]
    pat = r"|".join(pats)

    return re.sub(pat, f"{ST_PREFIX}URL", text, flags=re.I)


def get_precedents_dict(precedents_list=None):
    """
    Transforms a precedents list into a precedents dict
    creating STs as necessary.

    Precedents list format: ['pcdt10', 'pcdt11', '', 'pcdt21', 'pcdt22', ...].
    Each group of precedents is separated by an empty string.
    the first member of each group will be used as ST.

    Precedents dict format: precedents_dict[token] = group_st
    group_st is the st for the current group
    token is the token which will converge to a precedent
    """
    precedents_dict = dict()
    update_st_next = True

    if precedents_list is None:
        with open(PRECEDENTS_PREFIX_FILE) as f:
            precedents_list = list(map(str.strip, f.readlines()))

    for index, line in enumerate(precedents_list):
        if update_st_next:
            acronym, court = line.split("|")
            precedents_list[index] = acronym
            if court:
                precedent_st = f"{ST_PREFIX}{court.upper()}_{acronym.upper()}"
            else:
                precedent_st = "{ST_PREFIX}{{court}}{acronym}".format(
                    ST_PREFIX=ST_PREFIX, acronym=acronym.upper()
                )
            precedents_dict[acronym.lower()] = precedent_st
            update_st_next = False
        elif line:
            precedents_dict[line.lower()] = precedent_st
        else:
            update_st_next = True

    return precedents_dict


def get_precedents_regex(precedents_list=None):
    word_number = r"(?:número[s]?|n°s|ns\.|n\.o|n\.º|n2|no|nº|n\.|n)?"
    incident = fr"(\-{INCIDENTS})*"
    state = fr"(?:\s*(?:\w{{2}}|\\|\/|\-)\s*({STATES}|{FEDERAL_COURTS}))?"
    """
    Due to the following case found "AC 006003188.2008.4.01,9199/MG." where
    a comma exists between the numbers, the result of some cases
    like "(RESP 201700158919," will remove the comma at the end.
    """
    # numbers = r"(?P<number>[0-9\.\-,\/]+)"
    numbers = r"(?P<number>[0-9][0-9\.\-,\/]*[0-9]|[0-9])"

    if precedents_list is None:
        with open(PRECEDENTS_PREFIX_FILE) as f:
            precedents_list = list(map(str.strip, f.readlines()))

    precedents_list = filter(len, precedents_list)
    precedents_sorted = sorted(set(precedents_list), key=len, reverse=True)
    precedents_sorted = map(
        lambda x: r"\s+".join(x.split())
        .replace("(", "\\(")
        .replace(")", "\\)"),
        precedents_sorted,
    )
    precedents = r"|".join(precedents_sorted)
    precedents.replace("(", "\\(").replace(")", "\\)")
    acronym_group = fr"(?P<precedent>({precedents}))"
    precedents_regex = (
        fr"({acronym_group}\s*{word_number}\s*{numbers}{incident}{state})"
    )

    return precedents_regex


def transform_precedent(text, **kwargs):
    """
    Based on a list of precedents, create custom tokens for its appearances in a text.\n
    A .txt file with the list is necessary.

    :param text: Text which will have its precedents tokens created
    :type text: str
    :rtype: str
    :return: Text with the precedents tokens
    """

    with open(PRECEDENTS_PREFIX_FILE) as f:
        precedents_list = list(map(str.strip, f.readlines()))

    precedents_dict = get_precedents_dict(precedents_list)
    precedents_regex = get_precedents_regex(precedents_list)

    def get_precedent(type_precedent, number, court=""):
        precedent_acronym = precedents_dict[type_precedent.strip()]
        precedent_acronym = precedent_acronym.format(
            court=(court.upper() + "_" if court else "")
        )
        precedent = f"{precedent_acronym}_{number}"
        length_padding = 2

        return precedent.center(len(precedent) + length_padding)

    def transform(m):
        precedent = m.group("precedent")
        number = m.group("number").replace("/", "").replace(".", "")
        number = number.replace("-", "").replace(",", "")
        court = m.group("court") or ""

        try:
            return get_precedent(
                precedent.lower().replace("\n", " "), number, court
            )
        except KeyError:
            logging.warn("Unknown precedent: '%s'", precedent)
            return m.string

    return re.sub(
        fr"{BOUNDARY_BEG}{precedents_regex}{BOUNDARY_END}",
        transform,
        text,
        flags=re.I,
    )


def transform_named_entities_static(text, **kwargs):
    with open(NAMEDENTITIES_FILE) as f:
        entities = f.readlines()

    entities = set(e.strip().replace("-", r"\-").lower() for e in entities)

    return re.sub(
        r"\b(?<!-)(\S+)(?!-)\b",
        lambda m: (
            f" {ST_PREFIX}NAME " if m.group(1) in entities else m.group(1)
        ),
        text,
        flags=re.I,
    )


def get_known_words():
    """
    This function takes a vocabulary, removes unnecessary words and removes accents and cedillas from letters.

    :return: returns a list with the updated vocabulary
    :rtype: set[str]
    """
    known_words = get_vocabulary()
    known_words |= get_stopwords()

    # Considering with and without diacritic
    known_words |= set(
        map(lambda t: replace_diacritic(t.strip()), known_words)
    )

    return known_words


def transform_unknown(
    text, known_words=None, extra_known_words=tuple(), **kwargs
):
    """
    This function warns which words in the text are not in the defined vocabulary (Portuguese).

    * Ex.: palavras estrangeiras, palavras que não existem, palavras iniciadas com letras maiúsculas, etc.

    :param text: text that will be used for verification
    :type text: str
    :param known_words: words defined in vocabulary
    :type known_words: set[str]
    :param extra_known_words: extra words not in vocabulary
    :type extra_known_words: tuple
    :return: returns the text with warning which words do not exist in the vocabulary
    :rtype: str
    """
    if known_words is None:
        known_words = get_known_words()

    known_words |= set(
        map(lambda t: replace_diacritic(t.strip()), extra_known_words)
    )

    st = f"{ST_PREFIX}UNKNOWN "

    def replace_unknown(m):
        token = m.group("token")

        if not is_specialtoken(token) and token not in known_words:
            return f"{st}{token}"

        return token

    word = fr"(?:(?:-*[{PT_CHARS}_]-*)+)"

    return re.sub(
        fr"(?P<token>(?<!{st}){BOUNDARY_BEG}{word}{BOUNDARY_END})",
        replace_unknown,
        text,
        flags=re.I,
    )


def _synonym_to_st(token, special_laws=False):
    if not token:
        raise RuntimeError("Empty token to be used as ST for synonym")

    token = token.replace(" ", "_")
    if special_laws:
        special_token = f"{ST_PREFIX}LEI_{token}".upper()
    else:
        special_token = f"{ST_PREFIX}SYN_{token}".upper()

    return special_token


def preprocess_synonym(synonym):
    synonym = synonym.strip().lower()
    synonym = remove_extra_spaces(synonym)
    # synonym = replace_diacritic(synonym)

    return synonym


def get_synonyms_dict(
    synonyms_list=None,
    preprocess_fn=preprocess_synonym,
    is_preprocessed=True,
    special_laws=False,
):
    """
    Transforms an synonyms list to a synonyms dict,
    creating STs as necessary.

    Synonyms list format: ['syn10', 'syn11', '', 'syn21', 'syn22', ...].
    Each group of synonyms is separated by an empty string.
    The first member of each group will be used as ST.

    Synonyms dict format: synonyms_dict[token] = group_st
    group_st is the st for the current group
    token is token which has a synonym

    if synonyms_list is not given, it tries to read from a static file
    """
    preprocess_fn = str.strip if not is_preprocessed else preprocess_fn

    if special_laws:
        if synonyms_list is None:
            with open(SPECIAL_LAWS_FILE) as f:
                synonyms_list = map(preprocess_fn, f.readlines())
    else:
        if synonyms_list is None:
            with open(SYNONYMS_FILE) as f:
                synonyms_list = map(preprocess_fn, f.readlines())

    lines = iter(synonyms_list)
    synonyms_dict = dict()
    update_st_next = True

    for line in lines:
        if update_st_next:
            st = _synonym_to_st(line, special_laws)
            update_st_next = False

        if line:
            synonyms_dict[line.lower()] = st
        else:
            update_st_next = True

    return synonyms_dict


def get_synonyms_re(
    synonyms_list=None,
    synonyms_dict=None,
    is_preprocessed=True,
    special_laws=False,
):
    """Return compiled regex for synonyms.
    For optimization, too big to compile everytime
    """

    if synonyms_dict is None:
        synonyms_dict = get_synonyms_dict(
            synonyms_list,
            is_preprocessed=is_preprocessed,
            special_laws=special_laws,
        )

    synonyms = list(synonyms_dict.keys())
    synonyms.sort(reverse=True, key=len)

    synonyms = "|".join(synonyms)
    synonyms = synonyms.replace("(", "\\(")
    synonyms = synonyms.replace(")", "\\)")
    synonyms = synonyms.replace(" ", "\s")

    return re.compile(fr"{BOUNDARY_BEG}({synonyms}){BOUNDARY_END}", flags=re.I)


def transform_syn(
    text,
    synonyms_list=None,
    synonyms_dict=None,
    synonyms_re=None,
    is_preprocessed=True,
    special_laws=False,
    **kwargs,
):
    """
    Create custom tokens for synonyms words appearances in a text based in a static list. Basically, some common words can have synonyms in the text, so this function create tokens for these words.\n
    
    If the argument **synonyms_list** isn't passed in, a default .txt file with the list is needed.\n
    If the argument **synonyms_dict** isn't passed in, the dict will be created based from the synonyms_list.\n
    If the argument **synonyms_re** isn't passed in, the regex will be created based from the synonyms_dict.\n

    :param text: Text which will have its words analyzed and maybe tokens of synonyms created
    :type text: str
    :param synonyms_list: list of synonyms words (default is None)
    :type synonyms_list: list, optional
    :param synonyms_dict: dict of synonyms words (default is None)
    :type synonyms_dict: dict, optional
    :param synonyms_re: regex of synonyms (default is None)
    :type synonyms_re: str, optional
    :param is_preprocessed: if the text is already preprocessed (default is True)
    :type is_preprocessed: bool, optional
    :param special_laws: if the synonyms are related to special laws or "common words" (default is False)
    :type special_laws: bool, optional
    :param kwargs: is_text_preprocessed -- Condition if text is already preprocessed (default True), so synonyms will as well
    :rtype: str
    :return: The text with the synonyms tokens created (synonyms words with their tokens) will be returned
    """

    if synonyms_dict is None:
        synonyms_dict = get_synonyms_dict(
            synonyms_list=synonyms_list,
            is_preprocessed=is_preprocessed,
            special_laws=special_laws,
        )

    def replace(m):
        token = m.group(1)

        try:
            return synonyms_dict[token.lower().replace("\n", " ")]
        except KeyError:
            logging.debug(
                "'%s' token doesn't have synonym but was caught in"
                " text '%s'",
                token,
                text,
            )
            return token

    if synonyms_re is None:
        synonyms_re = get_synonyms_re(
            synonyms_list, synonyms_dict, is_preprocessed, special_laws
        )

    return synonyms_re.sub(replace, text)


def transform_special_laws(
    text,
    special_laws_list=None,
    special_laws_dict=None,
    special_laws_re=None,
    is_preprocessed=True,
    **kwargs,
):
    """
    Create custom tokens for special laws appearances in a text based in a static list. Basically, some common special laws can have synonyms (different words to represent them) in the text, so this function calls transform_syn with special_laws set as True so tokens for these special laws can be created.\n
    
    If the argument **special_laws_list** isn't passed in, a default .txt file with the list is needed.\n
    If the argument **special_laws_dict** isn't passed in, the dict will be created based from the special_laws_list.\n
    If the argument **special_laws_re** isn't passed in, the regex will be created based from the special_laws_dict.\n

    :param text: Text which will have its words analyzed and maybe tokens of special laws created
    :type text: str
    :param special_laws_list: list of special laws (default is None)
    :type special_laws_list: list, optional
    :param special_laws_dict: dict of special laws (default is None)
    :type special_laws_dict: dict, optional
    :param special_laws_re: regex of special laws (default is None)
    :type special_laws_re: str, optional
    :param is_preprocessed: if the text is already preprocessed (default is True)
    :type is_preprocessed: bool, optional
    :param kwargs: is_text_preprocessed -- Condition if text is already preprocessed (default True), so special laws will as well.
    :rtype: str
    :return: The text with the special laws tokens created will be returned
    """

    return transform_syn(
        text,
        synonyms_list=special_laws_list,
        synonyms_dict=special_laws_dict,
        synonyms_re=special_laws_re,
        is_preprocessed=is_preprocessed,
        special_laws=True,
    )


def transform_unify_legislation(text, **kwargs):
    """
    Unify legislation tokens.\n
    Basically, unify laws and articles from their tokens: ST_LEG_LEI_lawn°_ARTIGO_artn°

    :param text: Text which will have its tokens unified
    :type text: str
    :rtype: str
    :return: Text with the tokens unified

    *Note: Only works if remove_stop_words step is executed before*
    """


    caput = f"{ST_PREFIX}CAPUT"
    paragraph = fr"{ST_PREFIX}PARAGRAFO_(\d+|UNICO)"
    item = fr"{ST_PREFIX}INCISO_[MDCLXVI]+"
    indent = fr"{ST_PREFIX}ALINEA_\w+"
    previous_token = "ST_LEG_[\w+|\d+]+"
    ignored_expression = (
        "e?\d?seguintes",
        "e?\d?subsequentes",
        paragraph,
        item,
        indent,
        caput,
        previous_token,
    )
    ignored_expression = "|".join(ignored_expression)
    prefix_result = "LEG_"
    artigo = "(?P<artigo>st_artigo_\d+\w?)"
    lei = "(?P<lei>(?:st_lei_|st_decreto_|st_emenda_)(?:\d+|\w+))"

    def build_token(m):
        result = (
            ST_PREFIX
            + prefix_result
            + m.group("lei").replace("ST_", "")
            + "_"
            + m.group("artigo").replace("ST_", "")
            + " "
            + m.group("ignored_expression")
            + m.group("lei")
        )
        return result

    regex_unify = re.compile(
        fr"{artigo}\s+(?P<ignored_expression>(?:(?:{ignored_expression})\s+)*){lei}",
        flags=re.I,
    )
    while regex_unify.findall(text) != []:
        text = regex_unify.sub(build_token, text)

    def build_token_2(m):
        result = (
            m.group("lei")
            + " "
            + m.group("ignored_expression")
            + ST_PREFIX
            + prefix_result
            + m.group("lei").replace("ST_", "")
            + "_"
            + m.group("artigo").replace("ST_", "")
        )
        return result

    regex_unify_2 = re.compile(
        fr"{lei}\s+(?P<ignored_expression>(?:(?:{ignored_expression})\s+)*){artigo}",
        flags=re.I,
    )
    while regex_unify_2.findall(text) != []:
        text = regex_unify_2.sub(build_token_2, text)

    return text


def transform_extract_articles(text, **kwargs):
    """
    Extract the article from a legislation token (ST_LEG_LEI_law_ARTIGO_art)

    :param text: Text which will have its articles extracted from the token
    :type text: str
    :rtype: str
    :return: Text with the articles extracted
    """


    legislation_re = (
        fr"(?P<legislation>{ST_PREFIX}LEG_LEI_\w+_(?P<article>ARTIGO_\w+))"
    )

    text = re.sub(
        legislation_re,
        lambda m: (
            ST_PREFIX + m.group("article") + " " + m.group("legislation")
        ),
        text,
        flags=re.I,
    )

    return text


def correct_ocr_errors(text, **kwargs):
    """This function correct some ponctual OCR errors

    :param text: text that will have its OCR errors corrected
    :type text: str
    :rtype: str
    :return: Text with OCR errors corrected
    """
    with open(OCR_ERRORS_FILE) as f:
        ocr_errors_list = f.readlines()

    lines = iter(ocr_errors_list)
    ocr_errors_dict = dict()
    update_st_next = True

    for line in lines:
        line = line.replace("\n", "")
        if update_st_next:
            st = line
            update_st_next = False

        if line:
            ocr_errors_dict[line.lower()] = st
        else:
            update_st_next = True

    ocr_errors = list(ocr_errors_dict.keys())
    ocr_errors.sort(reverse=True, key=len)

    ocr_errors = "|".join(ocr_errors)
    ocr_errors = ocr_errors.replace("(", "\\(")
    ocr_errors = ocr_errors.replace(")", "\\)")

    def replace(m):
        token = m.group(1)

        try:
            return ocr_errors_dict[token.lower().replace("\n", " ")]
        except KeyError:
            logging.warn(
                "'%s' token doesn't have synonym but was caught in"
                " text '%s'",
                token,
                text,
            )
            return token

    ocr_erros_re = re.compile(
        fr"{BOUNDARY_BEG}({ocr_errors}){BOUNDARY_END}", flags=re.I
    )
    return ocr_erros_re.sub(replace, text)


def expand_abreviations(text, **kwargs):
    """This function expand juridical abreviations
    
    :param text: text that will have its juridical abbreviations expanded
    :type text: str
    :rtype: str
    :return: Text without juridical abbreviations
    """
    with open(ABREVIATIONS_FILE) as f:
        ocr_errors_list = f.readlines()

    lines = iter(ocr_errors_list)
    ocr_errors_dict = dict()
    update_st_next = True

    for line in lines:
        line = line.replace("\n", "")
        if update_st_next:
            st = line
            update_st_next = False

        if line:
            ocr_errors_dict[line.lower()] = st
        else:
            update_st_next = True

    ocr_errors = list(ocr_errors_dict.keys())
    ocr_errors.sort(reverse=True, key=len)

    ocr_errors = "|".join(ocr_errors)
    ocr_errors = ocr_errors.replace("(", "\\(")
    ocr_errors = ocr_errors.replace(")", "\\)")

    def replace(m):
        token = m.group(1)

        try:
            return ocr_errors_dict[token.lower().replace("\n", " ")]
        except KeyError:
            logging.warn(
                "'%s' token doesn't have synonym but was caught in"
                " text '%s'",
                token,
                text,
            )
            return token

    ocr_erros_re = re.compile(
        fr"{BOUNDARY_BEG}({ocr_errors}){BOUNDARY_END}", flags=re.I
    )
    return ocr_erros_re.sub(replace, text)
